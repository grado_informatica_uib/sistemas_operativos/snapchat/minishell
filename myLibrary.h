/**************************************************************************************************
*                                         MINISHELL - MYLYBRARY.H
*                                     SISTEMAS OPERATIVOS: AVENTURA 2.
*                                  GRADO EN INGENIERÍA INFORMÁTICA - UIB
*
*   AUTORES:     - JUAN CARLOS BOO CRUGEIRAS (carlosboocrugeiras@gmail.com)
*                - HÉCTOR MARIO MEDINA CABANELAS (hmedcab@gmail.com)
*   
*   FECHA:       02 DE DICIEMBRE DE 2018.
*
*   REPOSITORIO: https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell
*    (PRIVATE)
*
**************************************************************************************************/

/**************************************************************************************************
*                                         LIBRERÍAS NECESARIAS 
**************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h> 
#include <signal.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <fcntl.h>  



/**************************************************************************************************
*                                    DEFINIMOS CONSTANTES DEL PROGRAMA
**************************************************************************************************/

/*  CONSTANTES DE VISUALIZACIÓN. */
#define PROMPT             "$"             // CARACTER PROMPT.
#define SEPARATOR          "-->"           // SEPARADOR.
#define COLOR_BLUE         "\e[34;1m"      // COLOR AZUL.
#define COLOR_WHITE        "\e[37;0m"      // COLOR BLANCO.
#define COLOR_YELLOW       "\e[33;1m"      // COLOR AMARILLO.
#define COLOR_RED          "\e[31;1m"      // COLOR ROJO.



/* CONSTANTES TOKENS KEYS PERMITIDOS */
#define TOKEN_KEYS          " \t\n\r"   // DESCRIBE CARACTERES PARA TOKENIZAR.
#define COMMENT_WILDCARD    35          // ENTERO QUE SE CORRESPONDE CON '#'

/* DEFINICIONES RELACIONADOS CON LA LINEA INTRODUCIDA */
#define COMMAND_LINE_SIZE       1024    // DESCRIBE NÚMERO DE CARACTERES MÁXIMOS
#define ARGS_SIZE               64      // DESCRIBE NÚMERO DE ARGUMENTOS MÁXIMOS

/* DEFINICIONES RELACIONADAS CON LA PILA DE PROCESOS */
#define N_JOBS                  64      // NÚMERO MÁXIMO DE PROCESOS

struct info_process {                   // ESTRUCTURA DE LA PILA
    pid_t pid;
    char status;
    char command_line[COMMAND_LINE_SIZE];
};

static struct info_process jobs_list[N_JOBS];   // DECLARACIÓN DE LA PILA


/**************************************************************************************************
*                                    FUNCIONES A IMPLEMENTAR
**************************************************************************************************/

void imprimir_prompt();                         // IMPRIME EL PROMPT.
char *read_line(char *line);                    // LEE CARACTERES INTRODUCIDOS POR TECLADO.
int execute_line(char *line);                   // EJECUTA UN COMANDO.
int parse_args(char **args, char *line);        // TOKENIZA LA LÍNEA.
int check_internal(char **args);                // COMPRUEBA SI SE TRATA DE COMANDO INTERNO.
int internal_cd(char **args);                   // COMANDO INTERNO CD.
int internal_export(char **args);               // COMANDO INTERNO EXPORT.
int internal_source(char **args);               // COMANDO INTERNO SOURCE.
int internal_jobs(char **args);                 // COMANDO INTERNO JOBS.