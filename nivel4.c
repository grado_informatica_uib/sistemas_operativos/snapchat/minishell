/**************************************************************************************************
*                                           MINISHELL - NIVEL 4 
*                                     SISTEMAS OPERATIVOS: AVENTURA 2.
*                                  GRADO EN INGENIERÍA INFORMÁTICA - UIB
*
*   AUTORES:     - JUAN CARLOS BOO CRUGEIRAS (carlosboocrugeiras@gmail.com)
*                - HÉCTOR MARIO MEDINA CABANELAS (hmedcab@gmail.com)
*   
*   FECHA:       03 DE DICIEMBRE DE 2018.
*
*   REPOSITORIO: https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell
*    (PRIVATE)
*
**************************************************************************************************/

/*************************************************************************************************
*                   LIBRERÍAS NECESARIAS E INSTRUCCIONES PARA PREPROCESADOR.
**************************************************************************************************/
#define         _POSIX_C_SOURCE 200112L
#include        "myLibrary.h"
static char SHELL_COMMAND_LINE[] = "./nivel4"; //LÍNEA DE COMANDO QUE SE INTRODUCE PARA LLAMAR AL SHELL.
/*************************************************************************************************
*   NOMBRE:       IMPRIMIR_PROMPT();
*   ARGUMENTOS:   NINGUNO.
*   DEVUELVE:     NADA.
*   DESCRIPCIÓN:  LA FUNCIÓN IMPRIME UN ARRAY DE CHAR:
*                   - nombre_usuario-->directorio_actual$
**************************************************************************************************/
void imprimir_prompt(){
    // Reservamos espacio en memoria para el prompt.
    char *prompt = malloc(1000);
    // Añadimos color azul.
    strcpy(prompt, COLOR_BLUE);
    // Añadimos el nombre de usuario.
    strcat(prompt, getenv("USER"));
    // Añadimos color amarillo.
    strcat(prompt, COLOR_YELLOW);
    // Añadimos String separador.
    strcat(prompt, SEPARATOR);
    // Añadimos color rojo.
    strcat(prompt, COLOR_RED);
    // Añadimos directorio actual.
    strcat(prompt, getenv("PWD"));
    // Añadimos el caracter prompt.
    strcat(prompt, PROMPT);
    // Añadimos el color blanco.
    strcat(prompt, COLOR_WHITE);
    // Añadimos un espacio en blanco.
    strcat(prompt, " ");
    // imprimimos el prompt.
    printf("%s", prompt);
    // Liberamos espacio de memoria.
    free(prompt);
}


/*************************************************************************************************
*   NOMBRE:       READ_LINE()
*   ARGUMENTOS:   PUNTERO TIPO CHAR DE LA LÍNEA A LEER.
*   DEVUELVE:     DEVUELVE EL PUNTERO A LA LÍNEA LEÍDA.
*   DESCRIPCIÓN:  LA FUNCIÓN REALIZA LAS SIGUIENTES OPERACIONES:
*                   - IMPRIME EL PROMPT.
*                   - ALMACENA EN EL PUNTERO LINE LOS CARACTERES INTRODUCIDOS
*                   - DEVUELVE EL PUNTERO LINE.
**************************************************************************************************/
char *read_line(char *line){
    if(feof(stdin)){
        printf("\n");
        exit(0);
    }
    // Imprimimos el prompt.
    imprimir_prompt();
    // Alojamos en line los caracteres introducidos por teclado.
    fgets(line, COMMAND_LINE_SIZE, stdin);
    // Eliminamos el retorno de carro de line
    line[strlen(line)-1] = '\0';
    // Limpiamos el buffer de salida.
    fflush(stdout);
    // Limpiamos el buffer de entrada
    fflush(stdin);
    // Devolvemos el puntero.
    return line;
}

void reaper(int signum){
    // Asociamos a la señal SIGCHLD la función reaper
    signal(SIGCHLD, reaper);
    int status;
    pid_t pid = waitpid(-1, &status, WNOHANG);
    if(pid == jobs_list[0].pid){
        if(WTERMSIG(status)){
            printf("[reaper()→ Proceso hijo %d (%s) finalizado por señal %d]\n", jobs_list[0].pid, jobs_list[0].command_line, WTERMSIG(status));
        } else {
            // Informamos de lo ocurrido
            printf("[reaper()→ Proceso hijo %d (%s) finalizado con exit code %d]\n", jobs_list[0].pid, jobs_list[0].command_line, WEXITSTATUS(status));
        }
        // Indicamos que no hay proceso en primer plano.
        jobs_list[0].pid = 0;
        // Indicamos que el proceso que se está ejecutando es el shell.
        strcpy(jobs_list[0].command_line, SHELL_COMMAND_LINE);
        // Proceso finalizado.
        jobs_list[0].status = 'F';
    }
}


void ctrlc(int signum){
    // Asociamos a la señal SIGINT la función ctrlc para evitar incompatibilidad
    signal(SIGINT, ctrlc);
    printf("\n[ctrlc()→ Soy el proceso con PID %d (%s), el proceso en foreground es %d (%s)]\n", getpid(), SHELL_COMMAND_LINE, jobs_list[0].pid, jobs_list[0].command_line);
    if(jobs_list[0].pid > 0){
        if(strcmp(jobs_list[0].command_line, SHELL_COMMAND_LINE) != 0){
            kill(jobs_list[0].pid, SIGTERM);
            printf("[ctrlc()→ Señal %d enviada a %d (%s) por %d (%s)]\n", SIGTERM, jobs_list[0].pid, jobs_list[0].command_line, getpid(), SHELL_COMMAND_LINE);
        } else {
            printf("[ctrlc()→ Señal %d no enviada debido a que el proceso en foreground es el shell]\n", SIGTERM);
        }
    } else {
        printf("[ctrlc()→ Señal %d no enviada por %d debido a que no hay proceso en foreground]\n",SIGTERM, getpid());
    }
}

/*************************************************************************************************
*   NOMBRE:       EXECUTE_LINE()
*   ARGUMENTOS:   PUNTERO TIPO CHAR A LA LÍNEA INTRODUCIDA.
*   DEVUELVE:     DEVUELVE UN ENTERO QUE ES EL 'ESTADO' DE EJECUCIÓN.
*   DESCRIPCIÓN:  PRETENDER EJECUTAR EL COMANDO INTRODUCIDO.
*                   - CREAMOS DOBLE PUNTERO PARA ALMACENAR PUNTEROS A TOKENS.
*                   - LLAMAMOS A PARSE_ARGS PARA TOKENIZAR LINE.
*                   - LLAMAMOS A CHECK_INTERNAL() PARA DETERMINAR EL COMANDO.
**************************************************************************************************/
int execute_line(char *line){
    if(line == NULL || line[0] == '\0'){
        return 0;
    }
    // Creamos un doble puntero para almacenar los punteros a los tokens válidos
    char **args = malloc(ARGS_SIZE * sizeof(char *));
    // Hacemos una copia de la linea para guardarla en la pila de procesos.
    char command_line[COMMAND_LINE_SIZE];
    strcpy(command_line, line); 
    strtok(command_line, "\n");
    // Llamamos a parse_args(). Obtenemos args válido y número de tokens.
    int numTokens = parse_args(args, line);
    if(*args == NULL){
        return 0;
    }    

    // Llamamos a check_internal() y guardamos en state si se trata o no de
    // un comando válido.*args, ".."
    int state = check_internal(args);
    if(strcmp(command_line, "^C") == 0){
        return 0;
    }
    // Si no es un comando interno;
    if(state == 0){
        signal(SIGCHLD, reaper);
        // Creamos un hijo
        int childPID = fork();
        jobs_list[0].pid = childPID;
        jobs_list[0].status = 'E';
        strcpy(jobs_list[0].command_line, command_line);
        // Si se trata del hilo hijo
        if(childPID == 0){
            signal(SIGCHLD, SIG_DFL);
            signal(SIGINT, SIG_IGN);
            // Imprimimos el ID del padre.
            printf("[execute_line()→ PID padre: %d (%s)]\n", getppid(), SHELL_COMMAND_LINE);
            // Imprimimos el ID del hijo.
            printf("[execute_line()→ PID hijo: %d (%s)]\n", getpid(), jobs_list[0].command_line);
            // Creamos variable para almacenar argumentos.
            char *argv[ARGS_SIZE];
            // Contador de número de argumentos.
            int i = 0;           
            // Mientras haya argumentos.
            while(*args != NULL){
                // Asignamos el argumento al array.
                argv[i] = *args;
                // Avanzamos en el puntero
                args++;
                // Actualizamos el contador.
                i++;
            }
            // Por convenio el array ha de acabar con un elemento NULL indicando el final del array
            argv[i] = NULL;
            // Generamos el comando que es igual al primer elemento de los argumentos
            char *cmd = argv[0];
            // Ejecutamos el comando con los argumentos.
            execvp(cmd, argv);
            // Volvemos al principio del puntero
            args -= i;
            // Si el comando no existe.
            fprintf(stderr, "%s: no se encontró la orden \n", *args);
            // Si no habido errores, exit_success y si lo ha habito exit_failure.
            if(errno == 0){
                exit(EXIT_SUCCESS);
            }
            else{
                exit(EXIT_FAILURE);
            }
        }
        while(jobs_list[0].pid != 0){
            pause();
        }
    }
    // Liberamos espacio de memoria.
    free(args);
    // Devolvemos el estado.
    return state;
}


/*************************************************************************************************
*   NOMBRE:       PARSE_ARGS()
*   ARGUMENTOS:   1. DOBLE PUNTERO DE ARGUMENTOS.
*                 2. PUNTERO DE LA LÍNEA LEÍDA.
*   DEVUELVE:     NÚMERO DE TOKENS VÁLIDOS DE LA LÍNEA.
*   DESCRIPCIÓN:  LA FUNCIÓN PRETENDE TOKENIZAR LA LÍNEA QUE SE HA LEÍDO.
*                   - ALMACENAMOS PUNTEROS A TOKENS VÁLIDOS EN **ARGS
*                   - CONTAMOS NÚMERO DE TOKENS VÁLIDOS.
**************************************************************************************************/
int parse_args(char **args, char *line){
    // Declaramos e inicializamos número de tokens actuales.
    int tokenCounter = 0;
    // Guardamos en *args el primer elemento tokenizado de line
    *args = strtok(line, TOKEN_KEYS);
    // Imprimimos el comentario.
    ///printf("[parse_args()-> token %d: %s]\n", tokenCounter, *args);
    // Si *args no es nulo
    while(*args != NULL){
        // Si se trata de un comentario lo tratamos diferente.
         if(**args == COMMENT_WILDCARD){
            // Nos aseguramos lo que viene detrás no será tratado.
            *args = NULL;
            // Imprimimos mensaje de información.
            ///printf("[parse_args()-> token %d corregido: %s]\n", tokenCounter, *args);
        }
        // En caso de ser un token válido
        else {
            // Apuntamos hacia el siguiente puntero
            args++;
            // Aumentamos el contador de tokens.
            tokenCounter++;
            // Leemos el siguiente token y guardamos puntero en args.
            *args = strtok(NULL, TOKEN_KEYS);
            // Imprimimos el token.
            ///printf("[parse_args()-> token %d: %s]\n", tokenCounter, *args);
        }
    }
    // Devolvemos número de tokens.
    return tokenCounter;
}

/*************************************************************************************************
*   NOMBRE:       CHECK_INTERNAL()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     DEVUELVE UN ENTERO.
*   DESCRIPCIÓN:  COMPRUEBA SI EL COMANDO INTRODUCIDO EXISTE Y LLAMA A LA FUNCIÓN
*                 CORRESPONDIENTE.
**************************************************************************************************/
int check_internal(char **args){
    // Si no hay ningún token.
    if(*args == NULL){
        return 0;
    }
    // Si se ha introducido "exit".
    if(strcmp("exit", *args) == 0){  
        exit(0);
    }
    // Si se ha introducido "cd"
    if(strcmp("cd", *args) == 0){
        internal_cd(args);
        return 1;
    }
    // Si se ha introducido "export"
    if(strcmp("export", *args) == 0){
        internal_export(args);
        return 1;
    }
    // Si se ha introducido "source"
    if(strcmp("source", *args) == 0){
        internal_source(args);
        return 1;
    }
    // Si se ha introducido "jobs"
    if(strcmp("jobs", *args) == 0){
        internal_jobs(args);
        return 1;
    }
    return 0;
}

// Función auxiliar que sirve para concatenar arrays de chars.
void concatenateTokens(char **args, char *requestedDir){
    int boolean = 0;
    while(*args != NULL){
        int lastElement = strlen(*args)-1;    
        strcat(requestedDir, *args);
        int index = strlen(requestedDir);
        if(strchr(*args, 39) || strchr(*args, 34)){
            if(boolean == 1){
                boolean = 0;
            } else{
                boolean = 1;
            }
        }
        if(boolean == 1){
            strcat(requestedDir, " ");
        }
        args++;
    }
}


// Función auxiliar que permite reemplazar un caracter por otro en un 
// puntero de chars.
void replaceChar(char *string, int replacedChar){
    int i = 0;
    while(string[i] != '\0'){
        if(string[i] == replacedChar){
            string[i] = ' ';
        }
        i++;
    }
}


// Función auxiliar que permite sanear una cadena de caracteres eliminando
// el caracter " y '. 
void sanityString(char *string){
    int i = 0;
    int j = 0;
    int k = 0;
    while(string[i] != '\0'){
        if(string[i] == 34 || string[i] == 39){
            j = i;
            k = i + 1;
            while(string[k] != '\0'){
                string[j] = string[k];
                j++;
                k++;
            }
            string[j] = '\0';
        }
        i++;
   }
}


/*************************************************************************************************
*   NOMBRE:       INTERNAL_CD()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     
*   DESCRIPCIÓN:  REALIZA UN CAMBIO DE DIRECTORIO.
**************************************************************************************************/
int internal_cd(char **args){
    // Avanzamos uno en el puntero de argumentos, porque el nombre de la función
    // no nos interesa evaluarla.
    args++;
    // Directorio al que iremos. Lo rellenaremos debidamente con la información
    // solicitada por el usuario.
    char nextDir[COMMAND_LINE_SIZE];
    // Si el usuario no ha puesto ningún directorio, el directorio al que será
    // llevado será /home/"nombre de usuario"
    if(*args == NULL){
        strcpy(nextDir, "/home/");
        strcat(nextDir, getenv("USER"));
    // SI por el contrario ha puesto algo, evaluaremos ese directorio.
    } else {
        // Array de chars que contendrá el directorio solicitado. 
        char requestedDir[COMMAND_LINE_SIZE] = "";
        // Puntero de punteros a los argumentos
        char **requestedDirToken = malloc(ARGS_SIZE*sizeof(char *));
        // Número de tokens del directorio solicitado.
        int requestedDirNumToken = 0;
        // Número de ".." solicitados, esto es así porque tenemos que tratarlo
        // de forma apropipada para 'ir hacia atrás en el directorio actual 
        int wildcard = 0;
        // Array de chars del directorio actual.
        char actualDir[COMMAND_LINE_SIZE];
        // Puntero a punteros del directorio actual tokenizado.
        char **actualDirToken = malloc(ARGS_SIZE*sizeof(char *));
        // Número de tokens del directorio actual
        int actualDirNumToken = 0;

        // Copiamos el directorio actual al array actualDir
        strcpy(actualDir, getenv("PWD"));
        // Concatenamos los tokens solicitados y los guardamos en requestedDir
        concatenateTokens(args, requestedDir);
        // Reemplazamos los caracteres '\' con ' '
        replaceChar(requestedDir, 92);
        // Eliminamos de los arrays los caracteres " y '
        sanityString(requestedDir);
        // Cortamos los tokens según el caracter '/'
        *requestedDirToken = strtok(requestedDir, "/");

        // Ejecutamos un bucle para evaluar cuantos elementos tenemos
        while(*requestedDirToken != NULL){
            // Si el token es un wildcard tenemos que acumentar el contador. 
            if(strcmp(*requestedDirToken, "..") == 0 && strlen(*requestedDirToken) >1){
                wildcard++;
            }
            // Aumentamos el contador de tokens
            requestedDirNumToken++;
            // Apuntamos hacia el puntero del array.
            requestedDirToken++;
            // Copiamos el puntero en nuestro array de punteros. 
            *requestedDirToken = strtok(NULL, "/");
        }    

        // Volvemos al principio del array de punteros.
        requestedDirToken -= requestedDirNumToken;
        
        // Ejecutamos un bucle para evaluar cuantos elementos tenemos en el 

        // directorio actual
        *actualDirToken = strtok(actualDir, "/");
        while(*actualDirToken != NULL){
            // Aumentamos el contador de token del directorio acual
            actualDirNumToken++;
            // Avanzamos una posición en nuestro array de punteros.
            actualDirToken++;
            // Copiamos el puntero en nuestro array de punteros
            *actualDirToken = strtok(NULL, "/");
        }
        // Mientras tengamos wilcards
        while(wildcard > 0 ){
            // Restamos uno al contador de wildcards.
            wildcard--;
            // Restamos al número de directorios solicitados
            // Porque ya estamos tratando uno
            requestedDirNumToken--;
            // Restamos uno al contador de tokens del directorio actual
            // Porque hemos solicitado ir al directorio superior.
            actualDirNumToken--;
            // Avanzamos en el array de punteros de tokens solicitados.
            requestedDirToken++;
            // Vamos hacia atras en el array de punteros del directorio 
            // actual, porque trabajaremos a partir de ahí ahora. 
            actualDirToken--;
        }

        // Vamos al principio del directorio actual
        actualDirToken -= actualDirNumToken;
        // Copiamos la barra raíz en el directorio al que iremos.
        strcpy(nextDir, "/");
        // Si hay elementos en el directorio actual
        if(actualDirNumToken > 0){
            // Concatenamos el primer elemento del directorio actual
            strcat(nextDir, *actualDirToken);
        }
        // Mientras tengamos elementos en el directorio actual
        while(actualDirNumToken > 1){
            // Avanzamos en el array de punteros
            actualDirToken++;
            // Restamos uno al número de tokens del directorio actual
            actualDirNumToken--;
            // Concatenamos la barra al directorio al que iremos
            strcat(nextDir, "/");
            // Concatenamos el siguiente elemento al directorio al que iremos
            strcat(nextDir, *actualDirToken);
        }
        // Si el número de tokens solicitados es mayor a 0
        if(requestedDirNumToken > 0 && strcmp(*requestedDirToken, ".")){
            // Si la longitud del directorio es distinta de 1
            if(strlen(nextDir) != 1){
                // Concatenamos la barra raíz
                strcat(nextDir, "/");
            }
            // Concatenamos el primer elemento del directorio solicitado
            strcat(nextDir, *requestedDirToken);
        // Mientras haya tokens en el directorio solicitado
        while(requestedDirNumToken > 1){
            // Avanzamos en el array de punteros
            requestedDirToken++;
            // Restamos uno en el número de tokens porque ya hemos tratado uno
            requestedDirNumToken--;
            // Contactenamos la barra raiz
            strcat(nextDir, "/");
            // Concatenamos el elemento.
            strcat(nextDir, *requestedDirToken);

        }}
        }
    // Cambiamos al directorio solicitado
     if(chdir(nextDir) != -1){
         // Si no ha habido ningún error, modificamos la variable de entorno
         // PWD con el directorio actual
         setenv("PWD", nextDir, 1);
         // Imprimimos la información del directorio.
         /////printf("[internal_cd()-> %s]\n",nextDir);
        return 1;
    } 
    // Si ha habido algún error durante el cambio de directorio
    else {
        // Imprimimos el error.
        fprintf(stderr, "chdir: %s\n", strerror(errno));
        return 0;
    }
}


/*************************************************************************************************
*   NOMBRE:       INTERNAL_EXPORT()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     
*   DESCRIPCIÓN:  ASIGNARÁ VALORES A LAS VARIABLES DE ENTORNO.
**************************************************************************************************/
int internal_export(char **args){
    args++;
    // Si pones sólo 'source' salta este error.
    if(*args == NULL){
        fprintf(stderr,"Error de sintaxis. Uso: export Nombre=Valor \n");
        return -1;
    }
    // Copiamos la instrucción en la variable 'string'
    char string[COMMAND_LINE_SIZE];
    strcpy(string, *args);
    // Comprobamos que solo hay 2 tokens, si no, error
    args++;

    if(*args != NULL){
        fprintf(stderr,"Error de sintaxis. Uso: export Nombre=Valor \n");
        return -1;
    }
    // Troceamos la instruccion
    // Guardamos el string anterior al "=" en 'nombre'
    char nombre[COMMAND_LINE_SIZE];
    int i;
    for(i = 0; string[i] != '=' && string[i] != '\0'; i++){
        nombre[i] = string[i];
    }
    nombre[i] = '\0';
    // Si no hay "=" en el medio, es un error de sintaxis
    if(string[i] != '='){
        fprintf(stderr,"Error de sintaxis. Uso: export Nombre=Valor \n");
        return -1;
    }
    i++;
    // Guardamos el string posterior al "=" en 'valor'
    char valor[COMMAND_LINE_SIZE];
    int a;
    for(a = 0; string[i] != '\0'; a++){
        valor[a] = string[i];
        i++;
    }
    valor[a] = '\0';
    // Imprimimos por pantalla los cambios realizados
    /////printf("[internal_export() -> nombre: %s]\n", nombre);
    /////printf("[internal_export() -> valor: %s]\n", valor);
    /////printf("[internal_export() -> antiguo valor para %s: %s]\n", nombre, getenv(nombre));
    // Cambiamos el valor de la variable
    setenv(nombre, valor, 1);
    /////printf("[internal_export() -> nuevo valor para %s: %s]\n", nombre, getenv(nombre));
    return 1;
}


/*************************************************************************************************
*   NOMBRE:       INTERNAL_SOURCE()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     
*   DESCRIPCIÓN:  EJECUTARÁ UN FICHERO DE LÍNEAS DE COMANDOS.
**************************************************************************************************/
int internal_source(char **args){
  args++;
    if(*args != NULL){
        FILE * file = fopen(*args, "r");
        char linea[COMMAND_LINE_SIZE];

        if(file){
            char *pointer = fgets(linea, COMMAND_LINE_SIZE, (FILE*) file);
            fflush(file);
            while(pointer != NULL){
                printf("[internal_source()→ LINE: %s ]\n", pointer);
                execute_line(pointer);
                pointer = fgets(linea, COMMAND_LINE_SIZE, (FILE*) file);
                fflush(file);
            }
            fclose(file);
        } else {
            fprintf(stderr, "fopen: %s\n", strerror(errno));
        }
       
    } else{
        fprintf(stderr, "Error de sintaxis. Uso: source <nombre_fichero>\n");
    }
    return 1;  
}

/*************************************************************************************************
*   NOMBRE:       INTERNAL_JOBS()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     
*   DESCRIPCIÓN:  MOSTRARÁ EL PID DE LOS PROCESOS QUE NO ESTÉN EN BACKGROUND.
**************************************************************************************************/
int internal_jobs(char **args){
    printf("[internal_jobs()-> Esta función mostrará el PID de los procesos que no estén en foreground]\n");
    return 1;
}


void main(){
    char *line = malloc(COMMAND_LINE_SIZE);
     // Asociamos las señal SGCHLD a la función reaper.
    signal(SIGCHLD, reaper);
    // Asociamos las señal SIGINT a la función ctrlc
    signal(SIGINT, ctrlc);
    // Inicializamos la pila de procesos. 
    jobs_list[0].pid = 0;
    strcpy(jobs_list[0].command_line, "");
    jobs_list[0].status = 'E';
    // Mientras leamos la línea.
    while(read_line(line)){
        // Ejecutamos la linea.
        execute_line(line);
        // Limpiamos la línea de comandos.
        line[0] = '\0';
    }
}