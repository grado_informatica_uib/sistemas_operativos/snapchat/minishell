/**************************************************************************************************
*                                           MINISHELL - NIVEL 1 
*                                     SISTEMAS OPERATIVOS: AVENTURA 2.
*                                  GRADO EN INGENIERÍA INFORMÁTICA - UIB
*
*   AUTORES:     - JUAN CARLOS BOO CRUGEIRAS (carlosboocrugeiras@gmail.com)
*                - HÉCTOR MARIO MEDINA CABANELAS (hmedcab@gmail.com)
*   
*   FECHA:       02 DE DICIEMBRE DE 2018.
*
*   REPOSITORIO: https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell
*    (PRIVATE)
*
**************************************************************************************************/

/*************************************************************************************************
*                   LIBRERÍAS NECESARIAS E INSTRUCCIONES PARA PREPROCESADOR.
**************************************************************************************************/
#define         _POSIX_C_SOURCE 200112L
#include        "myLibrary.h"

/*************************************************************************************************
*   NOMBRE:       IMPRIMIR_PROMPT();
*   ARGUMENTOS:   NINGUNO.
*   DEVUELVE:     NADA.
*   DESCRIPCIÓN:  LA FUNCIÓN IMPRIME UN ARRAY DE CHAR:
*                   - nombre_usuario-->directorio_actual$
**************************************************************************************************/
void imprimir_prompt(){
    // Reservamos espacio en memoria para el prompt.
    char *prompt = malloc(1000);
    // Añadimos color azul.
    strcpy(prompt, COLOR_BLUE);
    // Añadimos el nombre de usuario.
    strcat(prompt, getenv("USER"));
    // Añadimos color amarillo.
    strcat(prompt, COLOR_YELLOW);
    // Añadimos String separador.
    strcat(prompt, SEPARATOR);
    // Añadimos color rojo.
    strcat(prompt, COLOR_RED);
    // Añadimos directorio actual.
    strcat(prompt, getenv("PWD"));
    // Añadimos el caracter prompt.
    strcat(prompt, PROMPT);
    // Añadimos el color blanco.
    strcat(prompt, COLOR_WHITE);
    // Añadimos un espacio en blanco.
    strcat(prompt, " ");
    // imprimimos el prompt.
    printf("%s", prompt);
    // Liberamos espacio de memoria.
    free(prompt);
}


/*************************************************************************************************
*   NOMBRE:       READ_LINE()
*   ARGUMENTOS:   PUNTERO TIPO CHAR DE LA LÍNEA A LEER.
*   DEVUELVE:     DEVUELVE EL PUNTERO A LA LÍNEA LEÍDA.
*   DESCRIPCIÓN:  LA FUNCIÓN REALIZA LAS SIGUIENTES OPERACIONES:
*                   - IMPRIME EL PROMPT.
*                   - ALMACENA EN EL PUNTERO LINE LOS CARACTERES INTRODUCIDOS
*                   - DEVUELVE EL PUNTERO LINE.
**************************************************************************************************/
char *read_line(char *line){
    // Imprimimos el prompt.
    imprimir_prompt();
    // Alojamos en line los caracteres introducidos por teclado.
    fgets(line, COMMAND_LINE_SIZE, stdin);
    // Eliminamos el retorno de carro de line
    line[strlen(line)-1] = '\0';
    // Limpiamos el buffer de salida.
    fflush(stdout);
    // Limpiamos el buffer de entrada
    fflush(stdin);
    // Devolvemos el puntero.
    return line;
}
/*************************************************************************************************
*   NOMBRE:       EXECUTE_LINE()
*   ARGUMENTOS:   PUNTERO TIPO CHAR A LA LÍNEA INTRODUCIDA.
*   DEVUELVE:     DEVUELVE UN ENTERO QUE ES EL 'ESTADO' DE EJECUCIÓN.
*   DESCRIPCIÓN:  PRETENDER EJECUTAR EL COMANDO INTRODUCIDO.
*                   - CREAMOS DOBLE PUNTERO PARA ALMACENAR PUNTEROS A TOKENS.
*                   - LLAMAMOS A PARSE_ARGS PARA TOKENIZAR LINE.
*                   - LLAMAMOS A CHECK_INTERNAL() PARA DETERMINAR EL COMANDO.
**************************************************************************************************/
int execute_line(char *line){
    // Creamos doble puntero
    char **args = malloc(ARGS_SIZE * sizeof(char *));
    // Guardamos en numTokens el número de tokens de line
    int numTokens = parse_args(args, line);
    // Comprobamos si se trata de un comando interno y si lo es se ejecuta.
    check_internal(args);
    // Liberamos memoria del puntero.
    free(args);
    // Devolvemos 0.
    return 0;
}

/*************************************************************************************************
*   NOMBRE:       PARSE_ARGS()
*   ARGUMENTOS:   1. DOBLE PUNTERO DE ARGUMENTOS.
*                 2. PUNTERO DE LA LÍNEA LEÍDA.
*   DEVUELVE:     NÚMERO DE TOKENS VÁLIDOS DE LA LÍNEA.
*   DESCRIPCIÓN:  LA FUNCIÓN PRETENDE TOKENIZAR LA LÍNEA QUE SE HA LEÍDO.
*                   - ALMACENAMOS PUNTEROS A TOKENS VÁLIDOS EN **ARGS
*                   - CONTAMOS NÚMERO DE TOKENS VÁLIDOS.
**************************************************************************************************/
int parse_args(char **args, char *line){
    // Declaramos e inicializamos número de tokens actuales.
    int tokenCounter = 0;
    // Guardamos en *args el primer elemento tokenizado de line
    *args = strtok(line, TOKEN_KEYS);
    // Imprimimos el comentario.
    printf("[parse_args()-> token %d: %s]\n", tokenCounter, *args);
    // Si *args no es nulo
    while(*args != NULL){
        // Si se trata de un comentario lo tratamos diferente.
         if(**args == COMMENT_WILDCARD){
            // Nos aseguramos lo que viene detrás no será tratado.
            *args = NULL;
            // Imprimimos mensaje de información.
            printf("[parse_args()-> token %d corregido: %s]\n", tokenCounter, *args);
        }
        // En caso de ser un token válido
        else {
            // Apuntamos hacia el siguiente puntero
            args++;
            // Aumentamos el contador de tokens.
            tokenCounter++;
            // Leemos el siguiente token y guardamos puntero en args.
            *args = strtok(NULL, TOKEN_KEYS);
            // Imprimimos el token.
            printf("[parse_args()-> token %d: %s]\n", tokenCounter, *args);
        }
    }
    // Devolvemos número de tokens.
    return tokenCounter;
}

/*************************************************************************************************
*   NOMBRE:       CHECK_INTERNAL()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     DEVUELVE UN ENTERO.
*   DESCRIPCIÓN:  COMPRUEBA SI EL COMANDO INTRODUCIDO EXISTE Y LLAMA A LA FUNCIÓN
*                 CORRESPONDIENTE.
**************************************************************************************************/
int check_internal(char **args){
    // Si no hay ningún token.
    if(*args == NULL){
        return 0;
    }
    // Si se ha introducido "exit".
    if(strcmp("exit", *args) == 0){  
        exit(0);
    }
    // Si se ha introducido "cd"
    if(strcmp("cd", *args) == 0){
        internal_cd(args);
        return 1;
    }
    // Si se ha introducido "export"
    if(strcmp("export", *args) == 0){
        internal_export(args);
        return 1;
    }
    // Si se ha introducido "source"
    if(strcmp("source", *args) == 0){
        internal_source(args);
        return 1;
    }
    // Si se ha introducido "jobs"
    if(strcmp("jobs", *args) == 0){
        internal_jobs(args);
        return 1;
    }
    return 0;
}

/*************************************************************************************************
*   NOMBRE:       INTERNAL_CD()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     
*   DESCRIPCIÓN:  REALIZA UN CAMBIO DE DIRECTORIO.
**************************************************************************************************/
int internal_cd(char **args){
    printf("[internal_cd()-> Esta función cambiará de directorio]\n");
    return 1;
}

/*************************************************************************************************
*   NOMBRE:       INTERNAL_EXPORT()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     
*   DESCRIPCIÓN:  ASIGNARÁ VALORES A LAS VARIABLES DE ENTORNO.
**************************************************************************************************/
int internal_export(char **args){
    printf("[internal_export()-> Esta función asignará valores a variables de entorno]\n");
    return 1;
}

/*************************************************************************************************
*   NOMBRE:       INTERNAL_SOURCE()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     
*   DESCRIPCIÓN:  EJECUTARÁ UN FICHERO DE LÍNEAS DE COMANDOS.
**************************************************************************************************/
int internal_source(char **args){
    printf("[internal_source()-> Esta función ejecutará un fichero de líneas de comandos]\n");
    return 1;
}

/*************************************************************************************************
*   NOMBRE:       INTERNAL_JOBS()
*   ARGUMENTOS:   PUNTERO A PUNTERO TIPO CHAR DE LOS TOKENS.
*   DEVUELVE:     
*   DESCRIPCIÓN:  MOSTRARÁ EL PID DE LOS PROCESOS QUE NO ESTÉN EN BACKGROUND.
**************************************************************************************************/
int internal_jobs(char **args){
    printf("[internal_jobs()-> Esta función mostrará el PID de los procesos que no estén en foreground]\n");
    return 1;
}


void main(){
    char *line = malloc(COMMAND_LINE_SIZE);
    while(read_line(line)){
        execute_line(line);
    }
}