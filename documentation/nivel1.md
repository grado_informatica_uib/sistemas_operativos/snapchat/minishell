# NIVEL 1.

Hay que leer de manera continua las líneas de comandos y trocearlas en tokens .Si el comando introducido en una línea es uno de los comandos internos (“cd”, “export”, “source” o “jobs”) imprimiremos por pantalla una frase indicando qué hará tal comando.

## READ_LINE()

## EXECUTE_LINE()

## PARSE_ARGS()

## CHECK_INTERNAL()

## INTERNAL_CD()

## INTERNAL_EXPORT()

## INTERNAL_SOURCE()

## INTERNAL_JOBS()